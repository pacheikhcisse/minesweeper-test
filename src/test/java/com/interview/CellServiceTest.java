package com.interview;

import com.interview.cell.Cell;
import com.interview.cell.CellException;
import com.interview.cell.CellService;
import com.interview.grid.Grid;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class CellServiceTest {

    private static final int GRID_WEIGHT = 3;
    private static final int GRID_HEIGHT = 3;

    private CellService sut;
    private Grid grid;

    @Before
    public void setUp() {
        sut = new CellService();
        grid = Grid.builder()
                .gridHeight(GRID_HEIGHT)
                .gridWeight(GRID_WEIGHT)
                .mineList(new ArrayList<>())
                .build();
    }

    @Test
    public void throwExceptionWhenUncoverCellOutsideGrid() {
        int outsideGridPosX = 4;
        int outsideGridPosY = 2;
        Cell cell = new Cell(outsideGridPosX, outsideGridPosY);
        Throwable thrown = Assert.assertThrows(CellException.class, () -> sut.uncoverCell(grid, cell));
        Assert.assertEquals("This cell is not in the grid boundaries", thrown.getMessage());
    }

    @Test
    public void throwExceptionWhenUncoverCellIsMine() {
        int mineCellPosX = 1;
        int mineCellPosY = 2;
        Cell aMineCell = new Cell(mineCellPosX, mineCellPosY);
        grid.getMineList().add(aMineCell);

        Cell cellToUncover = new Cell(mineCellPosX, mineCellPosY);
        Throwable thrown = Assert.assertThrows(MineSweeperException.class, () -> sut.uncoverCell(grid, cellToUncover));
        Assert.assertEquals("GAME OVER! You uncover a mine and you're dead :(", thrown.getMessage());
    }

}
