package com.interview;

import com.interview.grid.Grid;
import com.interview.grid.GridException;
import com.interview.grid.GridService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GridServiceTest {

    private static final int GRID_WEIGHT = 3;
    private static final int GRID_HEIGHT = 3;

    private GridService sut;

    @Before
    public void setUp() {
        sut = new GridService();
    }

    @Test(expected = GridException.class)
    public void throwExceptionWhenMoreMinesThanGrid() throws GridException {
        int nbMines = 10;
        sut.generateGrid(GRID_WEIGHT, GRID_HEIGHT, nbMines);
    }

    @Test
    public void shouldCreateGrid() throws GridException {
        //given
        int nbMines = 4;

        //when
        Grid actualGrid = sut.generateGrid(GRID_WEIGHT, GRID_HEIGHT, nbMines);

        //then
        Assert.assertEquals(GRID_WEIGHT, actualGrid.getGridWeight());
        Assert.assertEquals(GRID_HEIGHT, actualGrid.getGridHeight());
        Assert.assertNotNull(actualGrid.getMineList());
        Assert.assertEquals(nbMines, actualGrid.getMineList().size());
    }

}