package com.interview;

import com.interview.cell.Cell;
import com.interview.cell.CellException;
import com.interview.cell.CellService;
import com.interview.grid.Grid;
import com.interview.grid.GridException;
import com.interview.grid.GridService;

import java.util.Scanner;
import java.util.logging.Logger;

public class MineSweeper {

    private static final Logger LOG = Logger.getLogger(MineSweeper.class.getName());

    public static void main(String[] args) {
        LOG.info("Let's create the minesweeper grid");
        Grid grid = createGrid();

        LOG.info("Let's start uncovering the cells");
        grid = uncoverCell(grid);
    }

    private static Grid createGrid() {
        Grid grid = new Grid();
        boolean gridGenerated = false;
        do {
            try {
                grid = promptForGridCreation();
                gridGenerated = true;
            } catch (GridException e) {
                LOG.info(e.getMessage());
            }
        } while (!gridGenerated);
        LOG.info(String.format("A minesweeper (%s, %s) grid is created with %s mines%n",
                grid.getGridWeight(), grid.getGridHeight(), grid.getMineList().size()));
        return grid;
    }

    private static Grid uncoverCell(Grid grid) {
        boolean uncoveredCellInsideGrid = false;
        boolean gameOver = false;
        do {
            try {
                grid = promptForCellUncovering(grid);
            } catch (CellException e) {
                LOG.info(e.getMessage());
                uncoveredCellInsideGrid = true;
            } catch (MineSweeperException e) {
                LOG.info(e.getMessage());
                gameOver = true;
            }
        }
        while (!uncoveredCellInsideGrid && !gameOver);
        return grid;
    }

    private static Grid promptForGridCreation() throws GridException {
        Scanner sc = new Scanner(System.in);

        LOG.info("Give us grid weight: ");
        int gridWeight = sc.nextInt();

        LOG.info("The grid height: ");
        int gridHeight = sc.nextInt();

        LOG.info("And the grid number of mines: ");
        int nbMines = sc.nextInt();

        GridService gridService = new GridService();
        return gridService.generateGrid(gridWeight, gridHeight, nbMines);
    }

    private static Grid promptForCellUncovering(Grid grid) throws CellException, MineSweeperException {
        Scanner sc = new Scanner(System.in);

        LOG.info("Give us a cell posX: ");
        int posX = sc.nextInt();

        LOG.info("Give us a cell posY: ");
        int posY = sc.nextInt();

        Cell cell = new Cell(posX, posY);
        CellService cellService = new CellService();
        return cellService.uncoverCell(grid, cell);
    }

}
