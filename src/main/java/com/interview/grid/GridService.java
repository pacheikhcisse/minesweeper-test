package com.interview.grid;

import com.interview.cell.Cell;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class GridService {

    public Grid generateGrid(int gridWeight, int gridHeight, int nbMines) throws GridException {
        if (gridWeight * gridHeight < nbMines) {
            throw new GridException("There is too much mines for the grid");
        }
        Grid grid = Grid.builder()
                .gridHeight(gridHeight)
                .gridWeight(gridWeight)
                .mineList(new ArrayList<>())
                .build();
        return addMines(grid, nbMines);
    }

    private Grid addMines(Grid grid, int nbMines) {
        List<Integer> minesCellNumbers = new Random().ints(0, grid.getGridHeight() * grid.getGridWeight())
                .distinct()
                .limit(nbMines)
                .boxed()
                .collect(Collectors.toList());

        for (int mineCellNumber : minesCellNumbers) {
            int posX = mineCellNumber % grid.getGridWeight();
            int posY = mineCellNumber / grid.getGridWeight();
            Cell mineCell = new Cell(posX, posY);
            grid.getMineList().add(mineCell);
        }
        return grid;
    }

}