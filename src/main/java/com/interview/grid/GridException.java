package com.interview.grid;

public class GridException extends Exception {

    public GridException(String message) {
        super(message);
    }

}