package com.interview.grid;

import com.interview.cell.Cell;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Grid {

    private int gridWeight;
    private int gridHeight;
    private List<Cell> mineList;

}