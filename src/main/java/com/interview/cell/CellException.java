package com.interview.cell;

public class CellException extends Exception {

    public CellException(String message) {
        super(message);
    }
}
