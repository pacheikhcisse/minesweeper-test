package com.interview.cell;

import com.interview.MineSweeperException;
import com.interview.grid.Grid;

public class CellService {

    public Grid uncoverCell(Grid grid, Cell cell) throws CellException, MineSweeperException {
        if (!belongsToGrid(grid, cell)) {
            throw new CellException("This cell is not in the grid boundaries");
        }
        if (isMineUncovered(grid, cell)) {
            throw new MineSweeperException("GAME OVER! You uncover a mine and you're dead :(");
        }
        return grid;
    }

    private boolean belongsToGrid(Grid grid, Cell cell) {
        return cell.getPosX() < grid.getGridWeight()
                && cell.getPosY() < grid.getGridHeight();
    }

    private boolean isMineUncovered(Grid grid, Cell cell) {
        return grid.getMineList().contains(cell);
    }

}
