package com.interview;

public class MineSweeperException extends Exception {

    public MineSweeperException(String message) {
        super(message);
    }

}
